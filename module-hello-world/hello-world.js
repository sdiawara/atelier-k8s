const axios = require('axios');
const http = require('http');

const hostname = '0.0.0.0';
const port = 3000;


const server = http.createServer(async (req, res) => {
    try {
        const [response1, response2] = await axios.all([
            axios.get('http://hello:3000'),
            axios.get('http://world:3000')
        ]);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end(response1.data.toString() + ", " + response2.data.toString());
    } catch (error) {
        console.error(JSON.stringify(error.response));
        console.error(JSON.stringify(error));
        res.statusCode = 500;
        res.end(JSON.stringify(error.response));
    }
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
